import math_func
import pytest


def test_add():
    assert math_func.add(7,1) == 8
    result = math_func.add('Hello','World')
    assert result == 'HelloWorld'
    result = math_func.add(10.5,15.5)
    assert result == 263