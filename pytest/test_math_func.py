import math_func
import pytest
import sys


@pytest.mark.skipif(sys.version_info > (3,3),reason="Do not run add number test if python version is low")
def test_add():
    assert math_func.add(8) == 10
    assert math_func.add(4) == 6
    assert math_func.add(3) == 5


@pytest.mark.number
def test_product():
    print('========================================','lets start testing product', '========================================')
    assert math_func.product(5, 5) == 25
    assert math_func.product(6) == 12
    assert math_func.product(4) == 8


@pytest.mark.strings
def test_add_strings():
    result = math_func.add('Hello', 'World')
    assert result == 'HelloWorld'
    assert type(result) is str
    assert 'Hello' in result
    assert not result.islower()


@pytest.mark.strings
def test_product_strings():
    assert math_func.product('Hello', 3) == "HelloHelloHello"
