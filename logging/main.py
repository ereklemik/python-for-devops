import logging
logging.basicConfig(filename='demo.log', level=logging.WARNING, filemode='w', format='%(asctime)s => %(levelname)s => %(message)s')

# def nameCheck(name):
#     if len(name) < 2:
#         logging.debug('Check for name length')
#         return 'Invalid Name'
#     elif name.isspace():
#         logging.debug('Check if name is a space')
#         return('Invalid Name')
#     elif name.isalpha():
#         logging.debug('check if name is alphabet')
#         return('Name is valid')
#     else:
#         logging.debug('All Check failed')
#         return('Name is invalid')
    
# print(nameCheck('Erekle123'))

logging.debug('This is a DEBUG message')
logging.info('This is a INFO message')
logging.warning('This is a WARNING message')
logging.error('This is a ERROR message')
logging.critical('This is a CRITICAL message')
